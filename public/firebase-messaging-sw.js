

const askForPermissioToReceiveNotifications = async () => {
  const firebaseConfig = {
    apiKey: "AIzaSyCaSXK_rcPNjmMyryw92DWfAMiiKwl0HnY",
    authDomain: "acricana-5d993.firebaseapp.com",
    projectId: "acricana-5d993",
    storageBucket: "acricana-5d993.appspot.com",
    messagingSenderId: "540233012587",
    appId: "1:540233012587:web:23575dbb684cb1fe46e82e"
  };

  // Initialize Firebase
  const app = firebase.initializeApp(firebaseConfig);
  try {
      const messaging = firebase.messaging();
      await messaging.requestPermission();
      if ('serviceWorker' in navigator) {
        await navigator.serviceWorker.register('/service-worker.js')
        .then((reg) => {
          // registration worked
          console.log('Registration succeeded. ' + reg);
          messaging.useServiceWorker(reg); 
        }).catch((error) => {
          // registration failed
          
          console.log('Registration failed with ' + error);
        });
      }
      //const token = messaging.getToken({vapidKey: "BKbd1wPGY4UedafojsXmqJwAYvBcJeS-6XqYyTu92UcZU9_nRRhIht8abSwQicBMQhbnpl1w8LIvFO6gYvmvJgw"});
      messaging.getToken({ vapidKey: "BKbd1wPGY4UedafojsXmqJwAYvBcJeS-6XqYyTu92UcZU9_nRRhIht8abSwQicBMQhbnpl1w8LIvFO6gYvmvJgw" }).then((currentToken) => {
        if (currentToken) {
          // Send the token to your server and update the UI if necessary
          // ...
          console.log(currentToken)
          fetch('/set-token/'+userId+"/"+currentToken);
        } else {
          // Show permission request UI
          console.log('No registration token available. Request permission to generate one.');
          // ...
        }
        console.log(firebase.messaging.isSupported())
        messaging.onMessage((payload) => {
            console.log('Message received. ', payload);
            alertify.confirm(payload.data.title, payload.data.body, function(){
              if(payload.data.title == "Tienes un nuevo comunicado"){
                window.location.href = '/comunicados' 
              }else{
                console.log(payload)
                window.location.href = '/mensajes?alumnoId='+payload.data.alumnoId 
              }
            }, function(){ }).set('labels', {ok:'Ver', cancel:'Cancelar'}).set('closable', false);
            
            // ...
          });

         
      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        // ...
      });
      
    } catch (error) {
      console.error(error);
    }

   
}

askForPermissioToReceiveNotifications();


