<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\User;
use App\Entity\Comunicado;
use App\Entity\Archivos;
use App\Entity\Lectura;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Description of AppController
 *
 * @author Kelvins Insua
 */
class AppController extends Controller {
    /**
     * @Route("/", name="index")
     * @Method({"GET"})
     */
    public function login(Request $request) {
        return $this->render('auth-login.html.twig',[
            'message' => '',
            'message2' => ''
        ]);
    }

     /**
     *
     * @Route("/")
     * @Method({"POST"})
     */
    public function loginPost(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');
    
        $em = $this->getDoctrine()->getManager(); 
            $user = $em->getRepository(User::class)->createQueryBuilder('u')
                ->where('u.email = :username')
                ->setParameter('username', $username)
                ->andWhere("u.roles like '%ROLE_PADRE%'")
                ->getQuery()
                ->getOneOrNullResult();
            $valid = false;
            
            if($user instanceof User){
                $valid = password_verify($password, $user->getPassword());
            }
            if(!$user instanceof User || !$valid || !$user->isEnabled()){
                return $this->render('auth-login.html.twig',[
                    'message' => "Usuario o contraseña incorrectos.",
                    'message2' => ''
                ]);
            }
            $session = new Session();
            $session->start();

            // set and get session attributes
            $user->setUsernameCanonical($user->getNombre()." ".$user->getApellido());
            $session->set('user', $user);
            
            return $this->redirectToRoute('select-bu');
            
    }


    /**
     * @Route("/select-bu", name="select-bu")
     * @Method({"GET"})
     */
    public function buAction(Request $request) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $bdUser = $em->getRepository('App:User')->find($user->getId());
        $bu = null;
        if(count($bdUser->getBusinessUnits()) == 1){
            return $this->redirectToRoute('dashboard', [
                'bu' => $bdUser->getBusinessUnits()[0]->getId()
            ]);
        }else{
            $bu = $this->get('session')->get('bu');
            if($bu){
                return $this->redirectToRoute('dashboard', [
                    'bu' => $bu
                ]);
            }

            if($request->get('bu')){
                $valid = false;
                $session = $this->get('session');
                foreach($bdUser->getBusinessUnits() as $bu){
                    if($bu->getId() == $request->get('bu')){
                        $valid = true;
                        $session->set('logoUrl', $bu->getLogoUrl());
                    }
                }
                if(!$valid){
                    return $this->redirectToRoute('index');
                }
                return $this->redirectToRoute('dashboard', [
                    'bu' => $request->get('bu')
                ]);
            }
        }
        return $this->render('inicio.html.twig',[
        ]);
    }


    /**
     * @Route("/dashboard", name="dashboard")
     * @Method({"GET"})
     */
    public function dahsboardAction(Request $request) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $bdUser = $em->getRepository('App:User')->find($user->getId());
        $bu = null;
        $session = $this->get('session');
        if(count($bdUser->getBusinessUnits()) == 1){
            
            $bu = $bdUser->getBusinessUnits()[0]->getId();
            $session->set('bu', $bdUser->getBusinessUnits()[0]->getId());
            $session->set('logoUrl', $bdUser->getBusinessUnits()[0]->getLogoUrl());
        }else{
            if($request->get('bu')){
                $valid = false;
                foreach($bdUser->getBusinessUnits() as $bu){
                    if($bu->getId() == $request->get('bu')){
                        $valid = true;
                        $session->set('logoUrl', $bu->getLogoUrl());
                    }
                }
                if(!$valid){
                    return $this->redirectToRoute('index');
                }
                $bu = $request->get('bu');
                $session->set('bu', $request->get('bu'));
            }else{
                return $this->redirectToRoute('index');
            }
        }
        $alumnos = $em->getRepository('App:Alumno')->createQueryBuilder('a')
            ->innerJoin('a.padres', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $user->getId())
            ->andWhere('a.businessUnit = :bu')
            ->setParameter('bu', $bu)
            ->getQuery()
            ->getResult();
            $session->set('alumnos', $alumnos);
        return $this->render('index-acricana.html.twig',[
            'alumnos' => $alumnos
        ]);
        
        
    }

     /**
     *
     * @Route("/cerrar-session", name="cerrar_session_user")
     * @Method({"GET"})
     */
    public function cerrarSesionAction(Request $request) {
        $this->get('session')->clear();
        return $this->redirectToRoute('index');
    }

     /**
     *
     * @Route("/alumno/{id}", name="alumno")
     * @Method({"GET"})
     */
    public function alumnoAction(Request $request,$id) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $alumno = $em->getRepository('App:Alumno')->find($id);

        if($alumno){
            return $this->render('alumno.html.twig',[
                'alumno' => $alumno
            ]);

        }else{
            $bu = $this->get('session')->get('bu');
            if($bu){
                return $this->redirectToRoute('dashboard', [
                    'bu' => $bu
                ]);
            }else{
                return $this->redirectToRoute('index');
            }
        }
    }

    /**
     *
     * @Route("/comunicados", name="comunicados")
     * @Method({"GET"})
     */
    public function comunicadosAction(Request $request) {
        $user = $this->get('session')->get('user');
        $alumnoId = $request->get('alumnoId');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');
        $query = $em->getRepository('App:Comunicado')->createQueryBuilder('c');
        $query->where('c.aprobado = 1')
        ->andWhere('c.businessUnit = :bu')
            ->setParameter('bu', $bu)
            ->andWhere('c.parentId IS NULL')
            ->andWhere("c.seccion = 'comunicados'")
            ->orderBy('c.fechaCreacion', 'DESC')
            ->leftJoin('c.ciclos', 'ci')
            ->leftJoin('c.divisiones', 'd')
            ->leftJoin('c.cursos', 'cu')
            ->leftJoin('c.alumnos', 'a');

            if($alumnoId){
                $alumno = $em->getRepository('App:Alumno')->find($alumnoId);
                if(!$alumno){
                    return $this->redirectToRoute('index');
                }
                $alumnos = [$alumno];
            }else{
                $alumnos = $user->getAlumnos();
            }
            

            $alumnosIds = [];
            $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($alumnos as $a){
                $alumnosIds[] = $a->getId();
                $c = $a->getCursos();
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $orX = $query->expr()->orX();
            $orX->add($query->expr()->eq('c.todos', 1));
            $orX->add($query->expr()->in('a.id', $alumnosIds));
            $orX->add($query->expr()->in('cu.id', $cursosIds));
            $orX->add($query->expr()->in('ci.id', $ciclosIds));
            $orX->add($query->expr()->in('d.id', $divisionesIds));
            $query->andWhere($orX);

            $comunicados = $query->getQuery()->getResult();

            foreach($comunicados as $c){
                $cs = $em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->where('c.aprobado = 1')
                    ->andWhere('c.parentId = :parentId')
                    ->andWhere("c.seccion = 'comunicados'")
                    ->setParameter('parentId', $c->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
                $c->setRespuestas($cs);

                foreach($c->getLecturas() as $l){
                    if($l->getUsuario()->getId() == $user->getId()){
                        $c->setLeido(true);
                    }
                }

                
            }

            

            return $this->render('comunicados.html.twig',[
                'comunicados' => $comunicados
            ]);
    }

    /**
     *
     * @Route("/mensajes", name="mensajes")
     * @Method({"GET"})
     */
    public function mensajesAction(Request $request) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $alumnoId = $request->get('alumnoId');
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');
        $query = $em->getRepository('App:Comunicado')->createQueryBuilder('c');
        $query->where('c.creador = :user')
            ->setParameter('user', $user->getId())
            ->andWhere('c.parentId IS NULL')
            ->andWhere('c.alumno = :alumnoId')
            ->setParameter('alumnoId', $alumnoId)
            ->orderBy('c.id','DESC');

            $comunicados = $query->getQuery()->getResult();

            foreach($comunicados as $c){
                $cs = $em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->where('(c.aprobado = 1 AND c.creador != :user) OR (c.creador = :user)')
                    ->andWhere('c.parentId = :parentId')
                    ->andWhere("c.seccion = 'mensajes'")
                    ->setParameter('parentId', $c->getId())
                    ->setParameter('user', $user->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
                $c->setRespuestas($cs);

                foreach($c->getLecturas() as $l){
                    if($l->getUsuario()->getId() == $user->getId()){
                        $c->setLeido(true);
                    }
                }
                if($c->getLeido()){
                    foreach($cs as $cc){
                        $ls = $em->getRepository('App:Lectura')->findBy(['comunicado' => $cc->getId(), 'usuario' => $user->getId()]);
                        if(count($ls)==0){
                            $c->setLeido(false);
                            break;
                        }
                    }
                }
            }
            if($alumnoId){
                $alumno = $em->getRepository('App:Alumno')->find($alumnoId);
                if(!$alumno){
                    return $this->redirectToRoute('index');
                }
                $alumnos = [$alumno];
            }else{
                $alumnos = $user->getAlumnos();
            }
            

            $alumnosIds = [];
            $cursosIds = [];
            $divisionesIds = [];
            $ciclosIds = [];

            foreach($alumnos as $a){
                $alumnosIds[] = $a->getId();
                $c = $a->getCursos();
                $cursosIds[] = $c->getId();
                $divisionesIds[] = $c->getDivision()->getId();
                $ciclosIds[] = $c->getCiclo()->getId();
                
            }

            $profesores = count($cursosIds) > 0 ? $em->getRepository("App:User")->createQueryBuilder('u')
            ->innerJoin('u.materias', 'm','WITH', 'm.profesor = u.id')
            ->innerJoin('m.curso','c','WITH', 'm.curso = c.id')
            ->where('c.id in (:ids)')
            ->setParameter('ids', $cursosIds)
            ->getQuery()
            ->getResult() : [];

            return $this->render('mensajes.html.twig',[
                'comunicados' => $comunicados,
                'profesores' => $profesores
            ]);
    }


    /**
     *
     * @Route("/responder-comunicado/{id}", name="r-comunicados")
     * @Method({"POST"})
     */
    public function rcomunicadosAction(Request $request, $id) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');

        $comunicado = $em->getRepository('App:Comunicado')->find($id);
        $comunicado->setRespuestasNoLeidas($comunicado->getRespuestasNoLeidas() + 1);

        $c = new Comunicado();
        $c->setLeido(0);
        $c->setCreador($user);
        $c->setTipo($comunicado->getTipo());
        $c->setLeido(true);
        $c->setTodos(false);
        $c->setContenido($request->get('contenido'));
        $c->setTitulo($comunicado->getTitulo());
        $c->setFromAdmin(false);
        $c->setParentId($id);
        $c->setBusinessUnit($comunicado->getBusinessUnit());
        if($bu == 2){
            $c->setAprobado(true);
        }else{
            $c->setAprobado(true);
        }
        $em->persist($c);
        $em->flush();
        $file = $request->files->get('file');
        if($file){
            $a = new Archivos();
            $a->setNombre($file->getClientOriginalName());
            $a->setImageFile($file);
            $a->setComunicado($c);
            $em->persist($a);
            $em->flush();
        }        
        
        
        
        return $this->redirectToRoute('comunicados',[
            'alumnoId' => $request->get('alumnoId')
        ]);
    }


    /**
     *
     * @Route("/crear-mensaje", name="crear-mensaje")
     * @Method({"POST"})
     */
    public function cmensajesAction(Request $request) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');
        $docenteId = $request->get('docente');
        $alumnoId = $request->get('alumnoId');

        $c = new Comunicado();
        $c->setLeido(0);
        $c->setCreador($user);
        $c->setTipo('consulta');
        $c->setLeido(true);
        $c->setTodos(false);
        $c->setContenido($request->get('contenido'));
        $c->setTitulo($request->get('titulo'));
        $c->setFromAdmin(false);
        $bu = $em->getRepository('App:BusinessUnit')->find($bu);
        $c->setBusinessUnit($bu);
        $c->setSeccion('mensajes');
        if($bu->getId() == 1){
            $c->setAprobado(true);
        }else{
            $c->setAprobado(0);
        }
        if($docenteId){
            $u = $em->getRepository("App:User")->find($docenteId);
            $c->setDocente($u);
        }

        if($alumnoId){
            $u = $em->getRepository("App:Alumno")->find($alumnoId);
            $c->setAlumno($u);
            $c->addTodosCiclo($u->getCursos()->getCiclo());
        }
        $em->persist($c);
        $em->flush();
        $file = $request->files->get('file');
        if($file){
            $a = new Archivos();
            $a->setNombre($file->getClientOriginalName());
            $a->setImageFile($file);
            $a->setComunicado($c);
            $em->persist($a);
            $em->flush();
        }        
        
        
        
        return $this->redirectToRoute('mensajes',[
            'alumnoId' => $request->get('alumnoId')
        ]);
    }

     /**
     *
     * @Route("/responder-mensaje/{id}", name="r-mensajes")
     * @Method({"POST"})
     */
    public function rmensajesAction(Request $request, $id) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');
        $alumnoId = $request->get('alumnoId');
        $comunicado = $em->getRepository('App:Comunicado')->find($id);
        $comunicado->setRespuestasNoLeidas($comunicado->getRespuestasNoLeidas() + 1);
        $c = new Comunicado();
        $c->setLeido(0);
        $c->setCreador($user);
        $c->setTipo($comunicado->getTipo());
        $c->setLeido(true);
        $c->setTodos(false);
        $c->setContenido($request->get('contenido'));
        $c->setTitulo($comunicado->getTitulo());
        $c->setFromAdmin(false);
        $c->setParentId($id);
        $c->setSeccion('mensajes');
        $c->setBusinessUnit($comunicado->getBusinessUnit());
        if($bu == 1){
            $c->setAprobado(true);
        }else{
            $c->setAprobado(false);
        }
        if($alumnoId){
            $u = $em->getRepository("App:Alumno")->find($alumnoId);
            $c->setAlumno($u);
            $c->addTodosCiclo($u->getCursos()->getCiclo());
        }
        $em->persist($c);
        $em->flush();
        $file = $request->files->get('file');
        if($file){
            $a = new Archivos();
            $a->setNombre($file->getClientOriginalName());
            $a->setImageFile($file);
            $a->setComunicado($c);
            $em->persist($a);
            $em->flush();
        }     
        
        $l = new Lectura();
        $l->setUsuario($user);
        $l->setComunicado($c);
        $l->setAdmin(false);
        $l->setFecha(new \DateTime());

        $em->persist($l);
        $em->flush();
        
        
        
        return $this->redirectToRoute('mensajes',[
            'alumnoId' => $request->get('alumnoId')
        ]);
    }

     /**
     *
     * @Route("/set-token/{id}/{token}", name="t-comunicados")
     * @Method({"GET"})
     */
    public function tcomunicadosAction(Request $request, $id, $token) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $bu = $this->get('session')->get('bu');

        if(is_array($user->getFcm())){
            if(!in_array($token, $user->getFcm())){
                $arr = $user->getFcm();
                $arr[] = $token;
                $user->setFcm($arr);
            }
        }else{
            $user->setFcm([$token]);
        }

        $em = $em->flush(); 

        return new JsonResponse([
            'success' => true
        ]);
    }


    /**
     *
     * @Route("/crear-lectura/{id}", name="t-lectura")
     * @Method({"GET"})
     */
    public function lecturaAction(Request $request, $id) {
        $user = $this->get('session')->get('user');
        if(!$user){
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager(); 
        $user = $em->getRepository('App:User')->find($user->getId());
        $comunicado = $em->getRepository('App:Comunicado')->find($id);

        $ls = $em->getRepository('App:Lectura')->findBy(['comunicado' => $id, 'usuario' => $user->getId()]);

        if(count($ls) > 0){
            $cs = $em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->where('c.aprobado = 1')
                    ->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $comunicado->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
        foreach($cs as $cc){
            $ls = $em->getRepository('App:Lectura')->findBy(['comunicado' => $cc->getId(), 'usuario' => $user->getId()]);
            if(count($ls) > 0){
            }else{
                $l = new Lectura();
                $l->setUsuario($user);
                $l->setComunicado($cc);
                $l->setAdmin(false);
                $l->setFecha(new \DateTime());

                $em->persist($l);
                $em->flush();
            }
            
        }
            return new JsonResponse([
                'valid' => true
            ]);
        }

        $l = new Lectura();
        $l->setUsuario($user);
        $l->setComunicado($comunicado);
        $l->setAdmin(false);
        $l->setFecha(new \DateTime());

        $em->persist($l);
        $em->flush();

        $cs = $em->getRepository('App:Comunicado')->createQueryBuilder('c')
                    ->where('c.aprobado = 1')
                    ->andWhere('c.parentId = :parentId')
                    ->setParameter('parentId', $comunicado->getId())
                    ->orderBy('c.id','ASC')
                    ->getQuery()
                    ->getResult();
        foreach($cs as $cc){
            $ls = $em->getRepository('App:Lectura')->findBy(['comunicado' => $cc->getId(), 'usuario' => $user->getId()]);
            if(count($ls) > 0){
            }else{
                $l = new Lectura();
                $l->setUsuario($user);
                $l->setComunicado($cc);
                $l->setAdmin(false);
                $l->setFecha(new \DateTime());

                $em->persist($l);
                $em->flush();
            }
            
        }

        return new JsonResponse([
            'valid' => true,
            'cs' => count($cs)
        ]);

    }

    /**
     *
     * @Route("/cambiar/{token}")
     * @Method({"POST"})
     */
    public function cambiar(Request $request, $token) {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(array('confirmationToken' => $token));
        $userManager = $this->get('fos_user.user_manager');
        if ($user) {

            $new_pwd = $request->get('pass');
            $new_pwd1 = $request->get('pass2');

            if ($new_pwd != $new_pwd1) {
                return $this->render('auth-reset-password.html.twig',[
                    'user' => $user,
                    'message' => 'Las contraseñas no coinciden.',
                ]);
            }

            $user->setPlainPassword($new_pwd);
            $userManager->updateUser($user);




            return $this->render('auth-login.html.twig',[
                'message' => '',
                'message2' => 'Su contraseña ha sido recuperada exitosamente.',
            ]);
        }else{
            return $this->render('auth-login.html.twig',[
                'message' => '',
                'message2' => 'El usuario no existe.',
            ]);
        }
        
    }

     /**
     *
     * @Route("/recuperar-contrasena")
     * @Method({"POST"})
     */
    public function recoveryPass2(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(array('email' => $request->get('email')));
        $userManager = $this->get('fos_user.user_manager');
        if ($user) {
        $key = md5(uniqid());
        $user->setConfirmationToken($user->getEmail() . $key);
        $userManager->updateUser($user);
        $template = 'email/acricana-recuperar-contrasena.html.twig';
        if($user->getBusinessUnits()[0]->getId() == 1){
            $template = 'email/als-recuperar-contrasena.html.twig';
        }
        $message = (new \Swift_Message())
                    ->setSubject('Recuperar contraseña')
                    ->setFrom('noresponder@comunicados.acricana.org.ar')
                    ->setTo($user->getEmail())
                    ->setBody(
                    $this->get('templating')->render(
                            $template , array(
                            'base_url' => getenv('front_end_point'),
                            'user' => $user
                            )
                    ), 'text/html'
            );
            $this->get('mailer')->send($message);
        return $this->render('auth-login.html.twig',[
            'message' => '',
            'message2' => 'Se ha enviado un correo electrónico con la información para recuperar su cuenta.',
        ]);
        }else{
            return $this->render('auth-login.html.twig',[
                'message' => '',
                'message2' => 'Se ha enviado un correo electrónico con la información para recuperar su cuenta.',
            ]);
        }
    }

    /**
     *
     * @Route("/recuperar/{token}")
     * @Method({"GET"})
     */
    public function recuperar2(Request $request, $token) {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->findOneBy(array('confirmationToken' => $token));
        $userManager = $this->get('fos_user.user_manager');
        if ($user) {
        $userManager->updateUser($user);
        return $this->render('auth-reset-password.html.twig',[
            'user' => $user,
            'message' => '',
        ]);
        }else{
            return $this->render('auth-login.html.twig',[
                'message' => '',
                'message2' => 'El usuario no existe.',
            ]);
        }
        
    }
    /**
     *
     * @Route("/recuperar-contrasena")
     * @Method({"GET"})
     */
    public function recoveryPass(Request $request) {
        return $this->render('auth-forgot-password.html.twig',[
            'message' => '',
        ]);
    }
}
