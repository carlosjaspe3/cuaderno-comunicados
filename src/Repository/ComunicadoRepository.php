<?php

namespace App\Repository;

use App\Entity\Comunicado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comunicado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comunicado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comunicado[]    findAll()
 * @method Comunicado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComunicadoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comunicado::class);
    }

//    /**
//     * @return Comunicado[] Returns an array of Comunicado objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comunicado
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
