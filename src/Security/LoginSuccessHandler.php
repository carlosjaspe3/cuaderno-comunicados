<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginSuccessHandler
 *
 * @author USER
 */
namespace App\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $router;
    protected $authorizationChecker;

    public function __construct(Router $router, AuthorizationCheckerInterface $authorizationChecker) {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

        $response = null;

        $user = $token->getUser();
//        print $user->getUsername()." --- "; 
//        print $this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY');
//        die();
//        if ($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
//            $response = new RedirectResponse($this->router->generate('easyadmin').'/');
//        } else if ($this->authorizationChecker->isGranted('ROLE_USER')) {
//            $response = new RedirectResponse($this->router->generate('easyadmin').'');
//        }
        
        $response = new RedirectResponse($this->router->generate('easyadmin'));
        return $response;
    }

}