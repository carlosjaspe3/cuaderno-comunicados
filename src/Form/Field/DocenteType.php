<?php

namespace App\Form\Field;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;

class DocenteType extends AbstractType
{
    private $security;
    private $requestStack;
    protected $em;

    public function __construct(Security $security,RequestStack $requestStack,EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->security = $security;
        $this->requestStack = $requestStack;
    }

    /**
    * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        
        $resolver->setDefaults([
            'class' => 'App:User',
            'label' => false,
            'query_builder' => function(EntityRepository $er) {
                $qb = $er->createQueryBuilder('person');
                if($this->requestStack->getCurrentRequest()->get('entity') == 'MensajePA'){
                    $mensaje = $this->em->getRepository('App:Comunicado')->find($this->requestStack->getCurrentRequest()->get('id'));
                    $materias = [];
                    if($mensaje->getAlumno() && $mensaje->getAlumno()->getCursos() && $mensaje->getAlumno()->getCursos()->getMaterias()){
                        $materias = $mensaje->getAlumno()->getCursos()->getMaterias();
                    }
                    $materiaIds = [];
                    foreach($materias as $m){
                        $materiaIds[] = $m->getId();
                    }

                    $qb->join('person.materias', 'm')
                    ->where('m.id IN (:mids)')
                    ->setParameter('mids', $materiaIds);
                }
                return $qb
                    ->innerJoin('person.businessUnits', 'b')
                    ->andWhere("person.roles like '%ROLE_MAESTRO%'")
                    ->andWhere('b.id IN (:ids)')
                    ->setParameter('ids',$this->security->getUser()->getBusinessUnitsIds());
            },
            'attr' => array('data-widget' => 'select2'),
            'multiple' => false,
            'expanded'=> false,
        ]);
    }

    /**
    * @return string|null
    */
    public function getParent()
    {
        return EntityType::class;
    }
}