<?php

namespace App\Form\Field;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class DivisionesType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
    * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => 'App:Division',
            'label' => false,
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->andWhere('c.businessUnit IN (:ids)')
                    ->setParameter('ids',$this->security->getUser()->getBusinessUnitsIds());
            },
            'attr' => array('data-widget' => 'select2'),
            'multiple' => true,
            'expanded'=> false,
        ]);
    }

    /**
    * @return string|null
    */
    public function getParent()
    {
        return EntityType::class;
    }
}