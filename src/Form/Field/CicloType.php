<?php

namespace App\Form\Field;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class CicloType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
    * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => 'App:Ciclo',
            'label' => false,
            'query_builder' => function(EntityRepository $er) {
                $qb = $er->createQueryBuilder('c');
                if(!in_array('ROLE_ADMIN',$this->security->getUser()->getRoles())){
                    $qb->andWhere('c.id in (:cids)')
                    ->setParameter('cids',$this->security->getUser()->getCiclosIds());
                }
                return $qb->andWhere('c.businessUnit IN (:ids)')
                    ->setParameter('ids',$this->security->getUser()->getBusinessUnitsIds());
                    
            },
            'attr' => array('data-widget' => 'select2'),
            'multiple' => false,
            'expanded'=> false,
        ]);
    }

    /**
    * @return string|null
    */
    public function getParent()
    {
        return EntityType::class;
    }
}