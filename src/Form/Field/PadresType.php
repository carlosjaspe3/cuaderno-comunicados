<?php

namespace App\Form\Field;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PadresType extends AbstractType
{

    /**
    * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => 'App:User',
            'label' => false,
            'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('person')
                    ->where("person.roles like '%ROLE_PADRE%'");
            },
            'attr' => array('data-widget' => 'select2'),
            'multiple' => true,
            'expanded'=> false,
        ]);
    }

    /**
    * @return string|null
    */
    public function getParent()
    {
        return EntityType::class;
    }
}