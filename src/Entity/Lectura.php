<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LecturaRepository")
 */
class Lectura
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="lecturas")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Comunicado", inversedBy="lecturas")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $comunicado;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getComunicado(): ?Comunicado
    {
        return $this->comunicado;
    }

    public function setComunicado(?Comunicado $comunicado): self
    {
        $this->comunicado = $comunicado;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }
}
