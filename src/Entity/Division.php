<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DivisionRepository")
 */
class Division
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Curso", mappedBy="division")
     */
    private $cursos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessUnit", inversedBy="ciclos")
     */
    private $businessUnit;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Comunicado", mappedBy="divisiones")
     */
    private $comunicados;

    public function __construct()
    {
        $this->cursos = new ArrayCollection();
        $this->comunicados = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Curso[]
     */
    public function getCursos(): Collection
    {
        return $this->cursos;
    }

    public function addCurso(Curso $curso): self
    {
        if (!$this->cursos->contains($curso)) {
            $this->cursos[] = $curso;
            $curso->setDivision($this);
        }

        return $this;
    }

    public function removeCurso(Curso $curso): self
    {
        if ($this->cursos->contains($curso)) {
            $this->cursos->removeElement($curso);
            // set the owning side to null (unless already changed)
            if ($curso->getDivision() === $this) {
                $curso->setDivision(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return $this->getNombre();
    }
    
    public function getBusinessUnit(): ?BusinessUnit
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?BusinessUnit $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicados(): Collection
    {
        return $this->comunicados;
    }

    public function addComunicado(Comunicado $comunicado): self
    {
        if (!$this->comunicados->contains($comunicado)) {
            $this->comunicados[] = $comunicado;
            $comunicado->addDivisione($this);
        }

        return $this;
    }

    public function removeComunicado(Comunicado $comunicado): self
    {
        if ($this->comunicados->contains($comunicado)) {
            $this->comunicados->removeElement($comunicado);
            $comunicado->removeDivisione($this);
        }

        return $this;
    }
}
