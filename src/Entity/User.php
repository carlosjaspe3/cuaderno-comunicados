<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\HttpKernel\Exception\HttpException;
/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"user", "user-read"}},
 *     "denormalization_context"={"groups"={"user", "user-write"}}
 * })
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected $id;

    /**
     * @Groups({"user"})
     */
    protected $email;
    /**
     * @Groups({"user-write"})
     */
    protected $plainPassword;

    /**
     * @Groups({"user"})
     */
    protected $username;
    
    /**
     * @Groups({"user"})
     */
    protected $roles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Curso", mappedBy="preceptores")
     */
    private $cursos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Materia", mappedBy="profesor")
     */
    private $materias;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Alumno", mappedBy="padres")
     */
    private $alumnos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono2;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\BusinessUnit", inversedBy="users")
     */
    private $businessUnits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comunicado", mappedBy="creador")
     */
    private $comunicados;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fcm = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lectura", mappedBy="usuario")
     */
    private $lecturas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comunicado", mappedBy="docente")
     */
    private $comunicadosDocente;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ciclo", inversedBy="users")
     */
    private $ciclos;

    public function __construct()
    {
        parent::__construct();
        $this->cursos = new ArrayCollection();
        $this->materias = new ArrayCollection();
        $this->alumnos = new ArrayCollection();
        $this->businessUnits = new ArrayCollection();
        $this->comunicados = new ArrayCollection();
        $this->lecturas = new ArrayCollection();
        $this->comunicadosDocente = new ArrayCollection();
        $this->ciclos = new ArrayCollection();
    }

    public function __toString(){
        return $this->getNombre()." ".$this->getApellido();
    }

    /**
     * @return Collection|Curso[]
     */
    public function getCursos(): Collection
    {
        return $this->cursos;
    }

    public function addCurso(Curso $curso): self
    {
        if (!$this->cursos->contains($curso)) {
            $this->cursos[] = $curso;
            $curso->addPreceptore($this);
        }

        return $this;
    }

    public function removeCurso(Curso $curso): self
    {
        if ($this->cursos->contains($curso)) {
            $this->cursos->removeElement($curso);
            $curso->removePreceptore($this);
        }

        return $this;
    }

    /**
     * @return Collection|Materia[]
     */
    public function getMaterias(): Collection
    {
        return $this->materias;
    }

    public function addMateria(Materia $materia): self
    {
        if (!$this->materias->contains($materia)) {
            $this->materias[] = $materia;
            $materia->setProfesor($this);
        }

        return $this;
    }

    public function removeMateria(Materia $materia): self
    {
        if ($this->materias->contains($materia)) {
            $this->materias->removeElement($materia);
            // set the owning side to null (unless already changed)
            if ($materia->getProfesor() === $this) {
                $materia->setProfesor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Alumno[]
     */
    public function getAlumnos(): Collection
    {
        return $this->alumnos;
    }

    public function addAlumno(Alumno $alumno): self
    {
        if (!$this->alumnos->contains($alumno)) {
            $this->alumnos[] = $alumno;
            $alumno->addPadre($this);
        }

        return $this;
    }

    public function removeAlumno(Alumno $alumno): self
    {
        if ($this->alumnos->contains($alumno)) {
            $this->alumnos->removeElement($alumno);
            $alumno->removePadre($this);
        }

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getTelefono2(): ?string
    {
        return $this->telefono2;
    }

    public function setTelefono2(?string $telefono2): self
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    /**
     * @return Collection|BusinessUnit[]
     */
    public function getBusinessUnits(): Collection
    {
        return $this->businessUnits;
    }

    public function addBusinessUnit(BusinessUnit $businessUnit): self
    {
        if (!$this->businessUnits->contains($businessUnit)) {
            $this->businessUnits[] = $businessUnit;
        }

        return $this;
    }

    public function removeBusinessUnit(BusinessUnit $businessUnit): self
    {
        if ($this->businessUnits->contains($businessUnit)) {
            $this->businessUnits->removeElement($businessUnit);
        }

        return $this;
    }

    
   
    public function getBusinessUnitsIds()
    {
        $arr = [];

        foreach($this->getBusinessUnits() as $bu){
            $arr[] = $bu->getId();
        }

        //return join(",",$arr);
        return $arr;
    }

    public function getCiclosIds()
    {
        $arr = [];

        foreach($this->getCiclos() as $bu){
            $arr[] = $bu->getId();
        }

        //return join(",",$arr);
        return $arr;
    }

    public function getAlumnosIds()
    {
        $arr = [];

        foreach($this->getAlumnos() as $a){
            $arr[] = $a->getId();
        }

        //return join(",",$arr);
        return $arr;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicados(): Collection
    {
        return $this->comunicados;
    }

    public function addComunicado(Comunicado $comunicado): self
    {
        if (!$this->comunicados->contains($comunicado)) {
            $this->comunicados[] = $comunicado;
            $comunicado->setCreador($this);
        }

        return $this;
    }

    public function removeComunicado(Comunicado $comunicado): self
    {
        if ($this->comunicados->contains($comunicado)) {
            $this->comunicados->removeElement($comunicado);
            // set the owning side to null (unless already changed)
            if ($comunicado->getCreador() === $this) {
                $comunicado->setCreador(null);
            }
        }

        return $this;
    }

    public function getFcm(): ?array
    {
        return $this->fcm;
    }

    public function setFcm(?array $fcm): self
    {
        $this->fcm = $fcm;

        return $this;
    }

    /**
     * @return Collection|Lectura[]
     */
    public function getLecturas(): Collection
    {
        return $this->lecturas;
    }

    public function addLectura(Lectura $lectura): self
    {
        if (!$this->lecturas->contains($lectura)) {
            $this->lecturas[] = $lectura;
            $lectura->setUsuario($this);
        }

        return $this;
    }

    public function removeLectura(Lectura $lectura): self
    {
        if ($this->lecturas->contains($lectura)) {
            $this->lecturas->removeElement($lectura);
            // set the owning side to null (unless already changed)
            if ($lectura->getUsuario() === $this) {
                $lectura->setUsuario(null);
            }
        }

        return $this;
    } 


    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicadosDocente(): Collection
    {
        return $this->comunicadosDocente;
    }

    public function addComunicadosDocente(Comunicado $comunicadosDocente): self
    {
        if (!$this->comunicadosDocente->contains($comunicadosDocente)) {
            $this->comunicadosDocente[] = $comunicadosDocente;
            $comunicadosDocente->setDocente($this);
        }

        return $this;
    }

    public function removeComunicadosDocente(Comunicado $comunicadosDocente): self
    {
        if ($this->comunicadosDocente->contains($comunicadosDocente)) {
            $this->comunicadosDocente->removeElement($comunicadosDocente);
            // set the owning side to null (unless already changed)
            if ($comunicadosDocente->getDocente() === $this) {
                $comunicadosDocente->setDocente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Ciclo[]
     */
    public function getCiclos(): Collection
    {
        return $this->ciclos;
    }

    public function addCiclo(Ciclo $ciclo): self
    {
        if (!$this->ciclos->contains($ciclo)) {
            $this->ciclos[] = $ciclo;
        }

        return $this;
    }

    public function removeCiclo(Ciclo $ciclo): self
    {
        if ($this->ciclos->contains($ciclo)) {
            $this->ciclos->removeElement($ciclo);
        }

        return $this;
    }

    
    
}

