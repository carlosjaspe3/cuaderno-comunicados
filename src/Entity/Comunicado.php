<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComunicadoRepository")
 */
class Comunicado
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaCreacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comunicados")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $creador;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $leido;

    /**
     * @ORM\Column(type="integer")
     */
    private $aprobado;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Archivos", mappedBy="comunicado")
     */
    private $archivos;

    /**
     * @ORM\Column(type="boolean")
     */
    private $todos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ciclo", inversedBy="comunicados")
     */
    private $ciclos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Division", inversedBy="comunicados")
     */
    private $divisiones;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Curso", inversedBy="comunicados")
     */
    private $cursos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Alumno", inversedBy="comunicados")
     */
    private $alumnos;

    /**
     * @ORM\Column(type="text")
     */
    private $contenido;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fromAdmin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessUnit", inversedBy="comunicados")
     */
    private $businessUnit;

    private $respuestas;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $seccion = 'comunicados';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lectura", mappedBy="comunicado")
     */
    private $lecturas;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    private $destinatarios;

    /**
     * @ORM\Column(type="integer")
     */
    private $respuestasNoLeidas = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="comunicadosDocente")
     */
    private $docente;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Ciclo")
     * @ORM\JoinTable(name="comunicado_ciclos2",
     *      joinColumns={@ORM\JoinColumn(name="comunicado_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ciclo_id", referencedColumnName="id")}
     *      )
     */
    private $todosCiclos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Alumno", inversedBy="comunicadosAsignados")
     */
    private $alumno;

    public function __construct()
    {
        //$this->archivos = new ArrayCollection();
        $this->ciclos = new ArrayCollection();
        $this->divisiones = new ArrayCollection();
        $this->cursos = new ArrayCollection();
        $this->alumnos = new ArrayCollection();
        $this->fechaCreacion = new \DateTime();
        $this->respuestas = new ArrayCollection();
        $this->lecturas = new ArrayCollection();
        $this->todosCiclos = new ArrayCollection();
    }

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getCreador(): ?User
    {
        return $this->creador;
    }

    public function setCreador(?User $creador): self
    {
        $this->creador = $creador;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getLeido(): ?bool
    {
        return $this->leido;
    }

    public function setLeido(bool $leido): self
    {
        $this->leido = $leido;

        return $this;
    }

    public function getAprobado()
    {
        return $this->aprobado;
    }

    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;

        return $this;
    }

    
    public function getArchivos()
    {
        return $this->archivos;
    }

    public function addArchivo($archivo)
    {
        /* if (!$this->archivos->contains($archivo)) {
            $this->archivos[] = $archivo;
            $archivo->setComunicado($this);
        } */

        return $this;
    }

    public function removeArchivo($archivo)
    {
        if ($this->archivos->contains($archivo)) {
            $this->archivos->removeElement($archivo);
            // set the owning side to null (unless already changed)
            if ($archivo->getComunicado() === $this) {
                $archivo->setComunicado(null);
            }
        }

        return $this;
    }

    public function getTodos(): ?bool
    {
        return $this->todos;
    }

    public function setTodos(bool $todos): self
    {
        $this->todos = $todos;

        return $this;
    }

    /**
     * @return Collection|Ciclo[]
     */
    public function getCiclos(): Collection
    {
        return $this->ciclos;
    }

    public function addCiclo(Ciclo $ciclo): self
    {
        if (!$this->ciclos->contains($ciclo)) {
            $this->ciclos[] = $ciclo;
        }

        return $this;
    }

    public function removeCiclo(Ciclo $ciclo): self
    {
        if ($this->ciclos->contains($ciclo)) {
            $this->ciclos->removeElement($ciclo);
        }

        return $this;
    }

    /**
     * @return Collection|Division[]
     */
    public function getDivisiones(): Collection
    {
        return $this->divisiones;
    }

    public function addDivisione(Division $divisione): self
    {
        if (!$this->divisiones->contains($divisione)) {
            $this->divisiones[] = $divisione;
        }

        return $this;
    }

    public function removeDivisione(Division $divisione): self
    {
        if ($this->divisiones->contains($divisione)) {
            $this->divisiones->removeElement($divisione);
        }

        return $this;
    }

    /**
     * @return Collection|Curso[]
     */
    public function getCursos(): Collection
    {
        return $this->cursos;
    }

    public function addCurso(Curso $curso): self
    {
        if (!$this->cursos->contains($curso)) {
            $this->cursos[] = $curso;
        }

        return $this;
    }

    public function removeCurso(Curso $curso): self
    {
        if ($this->cursos->contains($curso)) {
            $this->cursos->removeElement($curso);
        }

        return $this;
    }

    /**
     * @return Collection|Alumno[]
     */
    public function getAlumnos(): Collection
    {
        return $this->alumnos;
    }

    public function addAlumno(Alumno $alumno): self
    {
        if (!$this->alumnos->contains($alumno)) {
            $this->alumnos[] = $alumno;
        }

        return $this;
    }

    public function removeAlumno(Alumno $alumno): self
    {
        if ($this->alumnos->contains($alumno)) {
            $this->alumnos->removeElement($alumno);
        }

        return $this;
    }

    public function getContenido(): ?string
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getFromAdmin(): ?bool
    {
        return $this->fromAdmin;
    }

    public function setFromAdmin(?bool $fromAdmin): self
    {
        $this->fromAdmin = $fromAdmin;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    public function getBusinessUnit(): ?BusinessUnit
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?BusinessUnit $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

   
    public function getRespuestas()
    {
        if($this->respuestas != null){
            return $this->respuestas;
        }
        return new ArrayCollection();
    }

    public function setRespuestas($respuestas): self
    {
        $this->respuestas = $respuestas;

        return $this;
    }

    public function getSeccion(): ?string
    {
        return $this->seccion;
    }

    public function setSeccion(string $seccion): self
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * @return Collection|Lectura[]
     */
    public function getLecturas(): Collection
    {
        return $this->lecturas;
    }

    public function addLectura(Lectura $lectura): self
    {
        if (!$this->lecturas->contains($lectura)) {
            $this->lecturas[] = $lectura;
            $lectura->setComunicado($this);
        }

        return $this;
    }

    public function removeLectura(Lectura $lectura): self
    {
        if ($this->lecturas->contains($lectura)) {
            $this->lecturas->removeElement($lectura);
            // set the owning side to null (unless already changed)
            if ($lectura->getComunicado() === $this) {
                $lectura->setComunicado(null);
            }
        }

        return $this;
    }

    public function getDestinatarios(): ?string
    {
        return $this->destinatarios;
    }

    public function setDestinatarios(?string $destinatarios): self
    {
        $this->destinatarios = $destinatarios;

        return $this;
    }

    public function getRespuestasNoLeidas(): ?int
    {
        return $this->respuestasNoLeidas;
    }

    public function setRespuestasNoLeidas(int $respuestasNoLeidas): self
    {
        $this->respuestasNoLeidas = $respuestasNoLeidas;

        return $this;
    }

    public function getDocente(): ?User
    {
        return $this->docente;
    }

    public function setDocente(?User $docente): self
    {
        $this->docente = $docente;

        return $this;
    }

    public function __toString(){
        return $this->getTitulo();
    }

    /**
     * @return Collection|Ciclo[]
     */
    public function getTodosCiclos(): Collection
    {
        return $this->todosCiclos;
    }

    public function addTodosCiclo(Ciclo $todosCiclo): self
    {
        if (!$this->todosCiclos->contains($todosCiclo)) {
            $this->todosCiclos[] = $todosCiclo;
        }

        return $this;
    }

    public function removeTodosCiclo(Ciclo $todosCiclo): self
    {
        if ($this->todosCiclos->contains($todosCiclo)) {
            $this->todosCiclos->removeElement($todosCiclo);
        }

        return $this;
    }

    public function getAlumno(): ?Alumno
    {
        return $this->alumno;
    }

    public function setAlumno(?Alumno $alumno): self
    {
        $this->alumno = $alumno;

        return $this;
    }


}
