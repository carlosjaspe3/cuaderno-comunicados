<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use \Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CursoRepository")
 * @Vich\Uploadable
 */
class Curso
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $horario;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="horario")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="cursos")
     */
    private $preceptores;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ciclo", inversedBy="cursos")
     */
    private $ciclo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Division", inversedBy="cursos")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $division;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Materia", mappedBy="curso")
     */
    private $materias;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessUnit", inversedBy="ciclos")
     */
    private $businessUnit;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaFin;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Comunicado", mappedBy="cursos")
     */
    private $comunicados;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Alumno", mappedBy="curso")
     */
    private $alumnos;

    public function __construct()
    {
        $this->preceptores = new ArrayCollection();
        $this->materias = new ArrayCollection();
        $this->alumnos = new ArrayCollection();
        $this->fechaInicio = new \DateTime();
        $this->fechaFin = new \DateTime();
        $this->comunicados = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getHorario(): ?string
    {
        return $this->horario;
    }

    public function setHorario(?string $horario): self
    {
        $this->horario = $horario;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPreceptores(): Collection
    {
        return $this->preceptores;
    }

    public function addPreceptore(User $preceptore): self
    {
        if (!$this->preceptores->contains($preceptore)) {
            $this->preceptores[] = $preceptore;
        }

        return $this;
    }

    public function removePreceptore(User $preceptore): self
    {
        if ($this->preceptores->contains($preceptore)) {
            $this->preceptores->removeElement($preceptore);
        }

        return $this;
    }

    
     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            $this->updatedAt = new \DateTime('now');
        }
        
    }
    
    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;        
    }

    public function __toString(){
        return $this->getNombre();
    }

    public function getCiclo(): ?Ciclo
    {
        return $this->ciclo;
    }

    public function setCiclo(?Ciclo $ciclo): self
    {
        $this->ciclo = $ciclo;

        return $this;
    }

    public function getDivision(): ?Division
    {
        return $this->division;
    }

    public function setDivision(?Division $division): self
    {
        $this->division = $division;

        return $this;
    }

    /**
     * @return Collection|Materia[]
     */
    public function getMaterias(): Collection
    {
        return $this->materias;
    }

    public function addMateria(Materia $materia): self
    {
        if (!$this->materias->contains($materia)) {
            $this->materias[] = $materia;
            $materia->setCurso($this);
        }

        return $this;
    }

    public function removeMateria(Materia $materia): self
    {
        if ($this->materias->contains($materia)) {
            $this->materias->removeElement($materia);
            // set the owning side to null (unless already changed)
            if ($materia->getCurso() === $this) {
                $materia->setCurso(null);
            }
        }

        return $this;
    }

    
    public function getBusinessUnit(): ?BusinessUnit
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?BusinessUnit $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->fechaInicio;
    }

    public function setFechaInicio(\DateTimeInterface $fechaInicio): self
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->fechaFin;
    }

    public function setFechaFin(\DateTimeInterface $fechaFin): self
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * @return Collection|Comunicado[]
     */
    public function getComunicados(): Collection
    {
        return $this->comunicados;
    }

    public function addComunicado(Comunicado $comunicado): self
    {
        if (!$this->comunicados->contains($comunicado)) {
            $this->comunicados[] = $comunicado;
            $comunicado->addCurso($this);
        }

        return $this;
    }

    public function removeComunicado(Comunicado $comunicado): self
    {
        if ($this->comunicados->contains($comunicado)) {
            $this->comunicados->removeElement($comunicado);
            $comunicado->removeCurso($this);
        }

        return $this;
    }

    /**
     * @return Collection|Alumno[]
     */
    public function getAlumnos(): Collection
    {
        return $this->alumnos;
    }

    public function addAlumno(Alumno $alumno): self
    {
        if (!$this->alumnos->contains($alumno)) {
            $this->alumnos[] = $alumno;
            $alumno->setCurso($this);
        }

        return $this;
    }

    public function removeAlumno(Alumno $alumno): self
    {
        if ($this->alumnos->contains($alumno)) {
            $this->alumnos->removeElement($alumno);
            // set the owning side to null (unless already changed)
            if ($alumno->getCurso() === $this) {
                $alumno->setCurso(null);
            }
        }

        return $this;
    }
}
